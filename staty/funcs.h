
#ifndef FUNCS_H_
#define FUNCS_H_

#include <vector>
#include <iostream>
#include <filesystem>


std::vector<std::filesystem::path> findDirs(std::string p);
std::filesystem::path findDir(std::string p, uint32_t n);
std::vector<std::filesystem::path> findFiles(std::filesystem::path p, std::string c);


#endif /* FUNCS_H_ */
#pragma once