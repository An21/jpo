/*
 * counter.cpp
 *
 *  Created on: 3 sty 2020
 *      Author: Admin
 */

#include "counter.h"
#include <iostream>
#include <string>
#include <filesystem>
using std::string;

void Counter::countChars(std::filesystem::path s)
{
	fstream f;
	f.open(s, std::ios::in);
	if (f.good() == false)
		isFile = false;

	string line;
	int cnt{};
	while (getline(f, line))
		cnt += line.size();

	f.close();
	charNmb = cnt;
}

void Counter::countWords(std::filesystem::path s)
{
	fstream f;
	f.open(s, std::ios::in);
	if (f.good() == false)
		isFile = false;

	string line;
	int cnt{};
	while (getline(f, line))
	{
		if (line[0] != '\0' && line[0] != ' ')
			cnt++;
		for (size_t i = 1; i < line.size(); i++)
			if (line[i] == ' ')
				if (line[i - 1] != ' ')
					cnt++;

	}
	f.close();
	wordNmb = cnt;
}

void Counter::countLines(std::filesystem::path s)
{
	fstream f;
	f.open(s, std::ios::in);
	if (f.good() == false)
		isFile = false;

	string line;
	int cnt{};
	while (getline(f, line))
		cnt++;

	f.close();
	lineNmb = cnt;
}

void Counter::countAll(std::filesystem::path s)
{
	fstream f;
	f.open(s, std::ios::in);
	if (f.good() == false)
		isFile = false;

	string line;
	int cntw{};
	int cntl{};
	int cntc{};
	while (getline(f, line))
	{
		cntl++;
		cntc += line.size();
		if (line[0] != '\0' && line[0] != ' ')
			cntw++;
		for (size_t i = 1; i < line.size(); i++)
			if (line[i] == ' ')
				if (line[i - 1] != ' ')
					cntw++;

	}
	f.close();
	wordNmb = cntw;
	lineNmb = cntl;
	charNmb = cntc;

}
