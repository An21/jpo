/*
 * counter.h
 *
 *  Created on: 8 sty 2020
 *      Author: Admin
 */

#ifndef COUNTER_H_
#define COUNTER_H_

#include <fstream>
#include <string>

using std::fstream;
using std::string;

class Counter
{
private:
	int charNmb{};
	int wordNmb{};
	int lineNmb{};
	bool isFile{};

public:
	int getCharNmb() { return charNmb; }
	int getWordNmb() { return wordNmb; }
	int getLineNmb() { return lineNmb; }

	void countChars(std::filesystem::path s);
	void countWords(std::filesystem::path s);
	void countLines(std::filesystem::path s);
	void countAll(std::filesystem::path s);

};





#endif /* COUNTER_H_ */
#pragma once
