#include "funcs.h"
#include <iostream>
#include <filesystem>

using std::string;

std::vector<std::filesystem::path> findDirs(string p)
{
	std::vector<std::filesystem::path> result;
	std::filesystem::recursive_directory_iterator end;
	for (auto& tmp : std::filesystem::recursive_directory_iterator(p))
		if (std::filesystem::exists(tmp.path()))
			if (std::filesystem::is_directory(tmp.path()))
			{
				//std::cout << tmp.path() << "\r\n";
				result.push_back(tmp.path());
			}
	return result;
}

std::filesystem::path findDir(string p, uint32_t n)
{
	for (const auto &tmp : std::filesystem::directory_iterator(p))
		if (std::filesystem::is_directory(tmp.path()))
		{
			n--;
			if (n == 0)
				return tmp.path();
		}
	return (std::filesystem::path)'\0';

}

std::vector<std::filesystem::path> findFiles(std::filesystem::path p, string c)
{
	std::vector<std::filesystem::path> result;
	for (const auto &tmp : std::filesystem::directory_iterator(p))
		if (tmp.path().filename().string().find(c) != string::npos)
			result.push_back(tmp.path());
	return result;
}