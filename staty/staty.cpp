//============================================================================
// Name        : staty.cpp
// Author      : cc
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include <iostream>
#include "counter.h"
#include <fstream>
#include <istream>
#include <iterator>
#include <string>
#include <filesystem>
#include <vector>
#include "funcs.h"

using std::string;
using std::fstream;



int main()
{
	fstream file{};
	string fileName{};							
	string path{};				// variables declarations

	int chars{};
	int words{};
	int lines{};
	int fNmb{};

	std::vector<std::filesystem::path> paths;
	std::vector<std::filesystem::path> files;			// containters for directory and files paths


Counter c;
char t{};
	for (;;)
	{
		std::cout << "Type 'C' to continue or 'X' to exit: ";
		std::cin >> t;
		std::cin.ignore();
		if (t == 'x' || t == 'X')
		{
			return 0;
		}
		else if (t == 'c' || t == 'C')							// simple user interface
		{
			std::cout << "Type path: ";
			std::getline(std::cin, path);
			if (std::filesystem::is_directory(path))
			{
				std::cout << "Type file format: ";
				std::getline(std::cin, fileName);
				paths = findDirs(path);						// find all directories in the path




				for (int i = 0; i < paths.size(); i++)
				{
					files = findFiles(paths[i], fileName);			// find all files matching pattern or extension
					for (int j = 0; j < files.size(); j++)
					{
						c.countAll(files[j]);
						chars += c.getCharNmb();
						words += c.getWordNmb();
						lines += c.getLineNmb();
						fNmb++;
						std::cout << "\r\n" << files[j] << "\r\n";		// display results
						std::cout << "File contains\n";
						std::cout << "Chars: " << c.getCharNmb() << "\nWords: " << c.getWordNmb() << "\nLines: " << c.getLineNmb() << "\n";
					}
				}
				if (fNmb != 0)
				{
					std::cout << "\r\nAll files contains\n";
					std::cout << "Chars: " << chars << "\nWords: " << words << "\nLines: " << lines << "\nFiles " << fNmb << "\n";
				}
				else
				{
					std::cout << "File not found\n";		// if no files founded 
				}
			}
			else
			{
				std::cout << "Incorect Path\n";			// if path does not exist
			}
		}
		else
		{

		}
	}

}
